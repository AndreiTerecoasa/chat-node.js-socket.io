$(document).ready(function() {
    var user = $("#username").text();
    var room_n = $("#room").text();
    var socket = io.connect( 'http://localhost:9090' );

    var users_connected = [];

    socket.emit('new user', {username: user, room: room_n});

    socket.on("user connected", function(data) {
        console.log(users_connected);
        users_connected = data.users_c;
        console.log(users_connected);
    })

    socket.on("user exists", function(data) {
        console.log(data);
        socket.disconnect();
    })


    $("#send_message").submit(function(e) {
        e.preventDefault();

        var msg = $("#message").val();
        socket.emit("new message", {message: msg});
    })

    socket.on("new message", function(data) {
        $("#messages-list").append($("<li><b>"+data.user+"</b>: "+data.msg+"</li>"));
    })
})
