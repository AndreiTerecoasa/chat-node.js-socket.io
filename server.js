var socket = require("socket.io");
var app = require("express")();
var http = require("http");

var server = http.createServer(app);

var io = socket.listen( server );
var users = [];
var rooms = [];

function user_in_room(user) {
    for(var i = 0; i < users.length; i++) {
        if((users[i].username == user.username) && (users[i].room == user.room)) {
            return true;
        }
    }
}

function disconnect_user(user) {
    for(var i = 0; i < users.length; i++) {
        if((users[i].username == user.username) && (users[i].room == user.room)) {
            users.splice(i, 1);
            return true;
        }
    }
}
io.sockets.on("connection", function(client) {
    console.log("New client !");
    client.on("new message", function(data) {
        io.to(client.room).emit("new message", {user: client.username, msg: data.message});
    })
    client.on("new user", function(data) {
        if(user_in_room(data)) {
            io.emit("user exists",{message: "User with same username already connected"});
        } else {
            client.username = data.username;
            client.room     = data.room;
            client.join(data.room);
            users.push({username: data.username, room: data.room});
            io.to(client.room).emit("user connected", {users_c: users});
            console.log(users);
            console.log("new user connected: " + data.username);
        }

    })

    client.on("disconnect", function(data) {
        if(!client.username && !client.room) return;
        disconnect_user(client);
        console.log(client.username + " disconnected from room "+ client.room);
        console.log(users);
    })

    client.on("new room", function(data) {
        // if(rooms.indexOf(data.roomName) != -1) {}
    })
})



server.listen( 9090 );
